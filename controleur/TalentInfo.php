<?php
    
include_once "../modele/TalentManage.php";

if (!isset($_GET["Nom"]))
{
include_once("../vue/entete.php");
include_once("../vue/Accueil.php");
include_once("../vue/pied.php");
}
else
{
$Init = new Talent;
$InfoTalent = $Init->getTalentByName($_GET["Nom"]);

include_once("../vue/entete.php");
echo str_replace(
    array("%%Nom%%","%%Description%%","%%back%%"),
    array($InfoTalent["Nom"],$InfoTalent["Description"],$_SERVER['HTTP_REFERER']),
    file_get_contents("../vue/vueTalentInfo.php"));
include_once("../vue/pied.php");
}

?>