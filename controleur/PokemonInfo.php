<?php
    
include_once "../modele/PkmnManage.php";

$Init = new Pkmn;

if (isset($_GET["N°"]))
{
	$_GET["Nom"]=$Init->NumToName($_GET["N°"]);
}

if (!isset($_GET["Nom"]))
{
include_once("../vue/entete.php");
include_once("../vue/vueLicense.php");
include_once("../vue/pied.php");
}
else
{

if (isset($_GET["form"]))
{
	$InfoPkmn = $Init->getPkmnByForm($_GET["Nom"], $_GET["form"]);
}
else
{
$InfoPkmn = $Init->getPkmnByName($_GET["Nom"]);
}
$Form="";
if ($Init->countForm($_GET["Nom"])>1)
{
	$Form = "| ";
	$InfoForm = $Init->getForm($_GET["Nom"]);
	foreach ($InfoForm as $Info)
	{
		$Form = $Form."<a href=\"../controleur/PokemonInfo?Nom=".$Info["Nom"]."&form=".$Info["Forme"]."\">".$Info["Forme"]."</a> | ";
	}
}

if (!$InfoPkmn)
{
	header("Location: ../controleur/Pokedex.php");
}
else
{
$HPB = $InfoPkmn["HP"]/2.55;
$AtkB = $InfoPkmn["Atk"]/2.55;
$DefB = $InfoPkmn["Def"]/2.55;
$AtkSB = $InfoPkmn["AtkS"]/2.55;
$DefSB = $InfoPkmn["DefS"]/2.55;
$VitB = $InfoPkmn["Vit"]/2.55;

include_once("../vue/entete.php");
echo str_replace(
    array("%%Nom%%","%%N°%%","%%Type%%","%%Type2%%","%%T1%%","%%T2%%","%%T3%%","%%TC%%","%%HP%%","%%Atk%%","%%Def%%","%%AtkS%%","%%DefS%%","%%Vit%%","*HP%*","*Atk%*","*Def%*","*AtkS%*","*DefS%*","*Vit%*","*Color1*","*Color2*","*Color3*","*Color4*","*Color5*","*Color6*","*Sprite*","*Prev*","*Next*","*Form*"),
    array($InfoPkmn["Nom"],$InfoPkmn["N°"],"<img src=\"../image/type/".$InfoPkmn["Type_1"].".png\"/>","<img src=\"../image/type/".Invisible($InfoPkmn["Type_2"])."\"/>",$InfoPkmn["Talent_1"],Secondary($InfoPkmn["Talent_2"]),Secondary($InfoPkmn["Talent_3"]),$InfoPkmn["Talent_TC"],$InfoPkmn["HP"],$InfoPkmn["Atk"],$InfoPkmn["Def"],$InfoPkmn["AtkS"],$InfoPkmn["DefS"],$InfoPkmn["Vit"],$HPB,$AtkB,$DefB,$AtkSB,$DefSB,$VitB,Color($InfoPkmn["HP"]),Color($InfoPkmn["Atk"]),Color($InfoPkmn["Def"]),Color($InfoPkmn["AtkS"]),Color($InfoPkmn["DefS"]),Color($InfoPkmn["Vit"]),$InfoPkmn["Sprite"],Previous($InfoPkmn["N°"]),Nextious($InfoPkmn["N°"]),$Form),
    file_get_contents("../vue/vuePokemonInfo.php"));
include_once("../vue/pied.php");
}
}



?>