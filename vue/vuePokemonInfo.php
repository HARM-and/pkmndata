        <div class="section"></div>
        <main>
            <center>
                <div class="container">
                    <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE; border-radius: 20px">
                        <div class='row'>
                    <table>
                      <tr>
                        <td class="left-align"><a href="../controleur/PokemonInfo?N°=*Prev*" class="brand-logo center"><img src="../image/TriG" id="N°" alt="Next" height="60px" width="70px"></a>*Prev*</td>
                        <td class="center-align"><h3>%%Nom%%</h3></td>
                        <td class="right-align">*Next*<a href="../controleur/PokemonInfo?N°=*Next*" class="brand-logo center"><img src="../image/TriD" id="N°" alt="Next" height="60px" width="70px"></a></td>

                      </tr>
                    </table>
                    <h4>%%N°%%</h4>
                    <h6>*Form*</h6>
                    <table>
   						    	<thead>
   						    		<tr>
    						        	<th width="50%"></th>
    						        	<th width="50%"></t>
    						      	</tr>
    						    </thead>
						
   						    	<tbody>
    						    	<tr>
            							<td><center><img src="../image/sprite/*Sprite*" alt="" height="100px" /></center></td>
           						 		<td><center><br>Talent : <a href="../controleur/TalentInfo?Nom=%%T1%%">%%T1%%</a>%%T2%%%%T3%%</center></td>
          							</tr>
          							<tr>
            							<td><center>%%Type%%%%Type2%%</center></td>
            							<td><center>Talent Caché : <a href="../controleur/TalentInfo?Nom=%%TC%%">%%TC%%</a></center></td>
          							</tr>
        						</tbody>
      						</table>
                            <table>
   						    	<thead>
   						    		<tr>
    						        	<th>Stat</th>
    						        	<th>Valeur</th>
    						        	<th width="1000px">Graph</th>
    						      	</tr>
    						    </thead>
						
   						    	<tbody>
    						    	<tr>
            							<td>HP</td>
           						 		<td><center>%%HP%%</center></td>
            							<td><div class="progress grey lighten-2" style="height: 15px"><div class="determinate *Color1* lighten-2" style="width: *HP%*%"></div></div></td>
          							</tr>
          							<tr>
            							<td>Atk</td>
            							<td><center>%%Atk%%</center></td>
            							<td><div class="progress grey lighten-2" style="height: 15px"><div class="determinate *Color2* lighten-2" style="width: *Atk%*%;"></div></td>
          							</tr>
          							<tr>
            							<td>Def</td>
            							<td><center>%%Def%%</center></td>
            							<td><div class="progress grey lighten-2" style="height: 15px"><div class="determinate *Color3* lighten-2" style="width: *Def%*%"></div></td>
          							</tr>
          							<tr>
            							<td>Atk Spé</td>
            							<td><center>%%AtkS%%</center></td>
            							<td><div class="progress grey lighten-2" style="height: 15px"><div class="determinate *Color4* lighten-2" style="width: *AtkS%*%"></div></td>
          							</tr>
          							<tr>
            							<td>Def Spé</td>
            							<td><center>%%DefS%%</center></td>
            							<td><div class="progress grey lighten-2" style="height: 15px"><div class="determinate *Color5* lighten-2" style="width: *DefS%*%"></div></td>
          							</tr>
          							<tr>
            							<td>Vit</td>
            							<td><center>%%Vit%%</center></td>
            							<td><div class="progress grey lighten-2" style="height: 15px"><div class="determinate *Color6* lighten-2" style="width: *Vit%*%"></div></td>
          							</tr>
        						</tbody>
      						</table>
                        </div>                           
                        <br />                            
                    </div>
                </div>
            </center>
            <div class="section"></div>
        </main>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
