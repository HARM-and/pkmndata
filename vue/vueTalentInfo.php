    <div class="section"></div>
        <main>
            <center>
                <div class="container">
                    <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE; border-radius: 20px">
                        <div class="center-align"><h2>%%Nom%%</h2></div>
                        <div>%%Description%%<br><br><br></div>
                    </div>
                </div>
                <a class='cyan-text' href="%%back%%">Page précédente</a>
            </center>
            <div class="section"></div>
        </main>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
