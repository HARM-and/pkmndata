        
        <div class="section"></div>
        <main>
            <center>
                <div class="container">
                    <div class="z-depth-1 lime lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE; border-radius: 20px">
                        <div class="left-align">

                        Copyright (C) 15/12/2020<br>
                        Créé par PERIGNON Armand [armand.perignon@laposte.net]<br><br>

                        This program is free software: you can redistribute it and/or modify<br>
                        it under the terms of the GNU General Public License as published by<br>
                        the Free Software Foundation, either version 3 of the License,<br>
                        or (at your option) any later version.<br><br>

                        This program is distributed in the hope that it will be useful,<br>
                        but WITHOUT ANY WARRANTY; without even the implied warranty of<br>
                        MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the<br>
                        GNU General Public License for more details.<br><br>
 
                        You should have received a copy of the GNU General Public License<br>
                        along with this program.  If not, see <a href="http://www.gnu.org/licenses/">http://www.gnu.org/licenses/</a>.<br><br>
                        
                        </div>
                    </div>
                </div>
                <a class='cyan-text' href="%%back%%">Page précédente</a>
            </center>
            <div class="section"></div>
        </main>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
        <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
