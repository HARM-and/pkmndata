<!DOCTYPE html>

<html>
    <head>
        <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
        <style>
        body {
            display: flex;
            min-height: 100vh;
            flex-direction: column;
        }

        main {
            flex: 1 0 auto;
        }

        body {
            background: #e4e4e4;
        }

        .input-field input[type=date]:focus + label,
        .input-field input[type=text]:focus + label,
        .input-field input[type=email]:focus + label,
        .input-field input[type=password]:focus + label {
        color: #e91e63;
        }

        .input-field input[type=date]:focus,
        .input-field input[type=text]:focus,
        .input-field input[type=email]:focus,
        .input-field input[type=password]:focus {
        border-bottom: 2px solid #e91e63;
        box-shadow: none;
        }
        
        nav.nav-center ul {
        text-align: center;
        }
        nav.nav-center ul li {
            display: inline;
            float: none;
        }
        nav.nav-center ul li a {
            display: inline-block;
        }
        </style>
    </head>
    
    <body>
        
        <nav>
          <div class="grey darken-3 nav-wrapper">
            <a class="brand-logo center" href="../controleur/Pokedex"><img class="responsive-img" style="width: 125px;" src="../image/pkbll.png" /></a>
            <ul id="nav-mobile" class="right hide-on-med-and-down">
            <form method="GET" action="../controleur/PokemonInfo">
                <li><div class="input-field right"><input id="nom" type="search" name="Nom" required><label class="label-icon" for="search"><i class="material-icons">search</i></label><i class="material-icons">close</i></div></li>
            </form>
            </ul>
          </div>
        </nav>

        
        