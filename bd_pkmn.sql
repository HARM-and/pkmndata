-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1:3306
-- Généré le :  mer. 16 déc. 2020 à 15:29
-- Version du serveur :  10.4.10-MariaDB
-- Version de PHP :  7.4.0

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données :  `bd_pkmn`
--

-- --------------------------------------------------------

--
-- Structure de la table `pokemon`
--

DROP TABLE IF EXISTS `pokemon`;
CREATE TABLE IF NOT EXISTS `pokemon` (
  `Nom` varchar(30) NOT NULL,
  `Forme` varchar(30) NOT NULL DEFAULT 'Basique',
  `N°` varchar(8) NOT NULL,
  `Type_1` varchar(30) DEFAULT NULL,
  `Type_2` varchar(30) DEFAULT NULL,
  `Talent_1` varchar(30) DEFAULT NULL,
  `Talent_2` varchar(30) DEFAULT NULL,
  `Talent_3` varchar(30) DEFAULT NULL,
  `Talent_TC` varchar(30) DEFAULT NULL,
  `HP` int(11) NOT NULL DEFAULT 0,
  `Atk` int(11) NOT NULL DEFAULT 0,
  `Def` int(11) NOT NULL DEFAULT 0,
  `AtkS` int(11) NOT NULL DEFAULT 0,
  `DefS` int(11) NOT NULL DEFAULT 0,
  `Vit` int(11) NOT NULL DEFAULT 0,
  `Sprite` varchar(60) NOT NULL DEFAULT 'unknown.png',
  PRIMARY KEY (`Nom`,`Forme`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `pokemon`
--

INSERT INTO `pokemon` (`Nom`, `Forme`, `N°`, `Type_1`, `Type_2`, `Talent_1`, `Talent_2`, `Talent_3`, `Talent_TC`, `HP`, `Atk`, `Def`, `AtkS`, `DefS`, `Vit`, `Sprite`) VALUES
('Bulbizarre', 'Basique', '001', 'Plante', 'Poison', 'Engrais', NULL, NULL, 'Chlorophylle', 45, 49, 49, 65, 65, 45, 'Sprite_001.png'),
('Herbizarre', 'Basique', '002', 'Plante', 'Poison', 'Engrais', NULL, NULL, 'Chlorophylle', 60, 62, 63, 80, 80, 60, 'Sprite_002.png'),
('Florizarre', 'Basique', '003', 'Plante', 'Poison', 'Engrais', NULL, NULL, 'Chlorophylle', 80, 82, 83, 100, 100, 80, 'Sprite_003.png'),
('Salamèche', 'Basique', '004', 'Feu', NULL, 'Brasier', NULL, NULL, 'Force Soleil', 39, 52, 43, 60, 50, 65, 'Sprite_004.png'),
('Reptincel', 'Basique', '005', 'Feu', NULL, 'Brasier', NULL, NULL, 'Force Soleil', 58, 64, 58, 80, 65, 80, 'Sprite_005.png'),
('Dracaufeu', 'Basique', '006', 'Feu', 'Vol', 'Brasier', NULL, NULL, 'Force Soleil', 78, 84, 78, 109, 85, 100, 'Sprite_006.png'),
('Carapuce', 'Basique', '007', 'Eau', NULL, 'Torrent', NULL, NULL, 'Cuvette', 44, 48, 65, 50, 64, 43, 'Sprite_007.png'),
('Carabaffe', 'Basique', '008', 'Eau', NULL, 'Torrent', NULL, NULL, 'Cuvette', 59, 63, 80, 65, 80, 58, 'Sprite_008.png'),
('Tortank', 'Basique', '009', 'Eau', NULL, 'Torrent', NULL, NULL, 'Cuvette', 79, 83, 100, 85, 105, 78, 'Sprite_009.png'),
('Chenipan', 'Basique', '010', 'Insecte', NULL, 'Ecran Poudre', NULL, NULL, 'Fuite', 45, 30, 35, 20, 20, 45, 'Sprite_010.png'),
('Chrysacier', 'Basique', '011', 'Insecte', NULL, 'Mue', NULL, NULL, NULL, 50, 20, 55, 25, 25, 30, 'Sprite_011.png'),
('Papilusion', 'Basique', '012', 'Insecte', 'Vol', 'Œil Composé', NULL, NULL, 'Lentiteinté', 60, 45, 50, 90, 80, 70, 'Sprite_012.png'),
('Aspicot', 'Basique', '013', 'Insecte', 'Poison', 'Ecran Poudre', NULL, NULL, 'Fuite', 40, 35, 30, 20, 20, 50, 'Sprite_013.png'),
('Coconfort', 'Basique', '014', 'Insecte', 'Poison', 'Mue', NULL, NULL, NULL, 45, 25, 50, 25, 25, 35, 'Sprite_014.png'),
('Dardargnan', 'Basique', '015', 'Insecte', 'Poison', 'Essaim', NULL, NULL, 'Sniper', 65, 80, 40, 45, 80, 75, 'Sprite_015.png'),
('Roucool', 'Basique', '016', 'Normal', 'Vol', 'Regard Vif', 'Pieds Confus', NULL, 'Cœur de Coq', 40, 45, 40, 35, 35, 46, 'Sprite_016.png'),
('Roucoups', 'Basique', '017', 'Normal', 'Vol', 'Regard Vif', 'Pieds Confus', NULL, 'Cœur de Coq', 63, 60, 55, 50, 50, 71, 'Sprite_017.png'),
('Roucarnage', 'Basique', '018', 'Normal', 'Vol', 'Regard Vif', 'Pieds Confus', NULL, 'Cœur de Coq', 83, 80, 75, 70, 70, 101, 'Sprite_018.png'),
('Rattata', 'Basique', '019', 'Normal', NULL, 'Fuite', 'Cran', NULL, 'Agitation', 30, 56, 35, 25, 35, 72, 'Sprite_019.png'),
('Rattatac', 'Basique', '020', 'Normal', NULL, 'Fuite', 'Cran', NULL, 'Agitation', 55, 81, 60, 50, 70, 97, 'Sprite_020.png'),
('Piafabec', 'Basique', '021', 'Normal', 'Vol', 'Regard Vif', NULL, NULL, 'Sniper', 40, 60, 30, 31, 31, 70, 'Sprite_021.png'),
('Rapasdepic', 'Basique', '022', 'Normal', 'Vol', 'Regard Vif', NULL, NULL, 'Sniper', 65, 90, 65, 61, 61, 100, 'Sprite_022.png'),
('Regieleki', 'Basique', '894', 'Electrik', NULL, 'Transistor', NULL, NULL, NULL, 80, 10, 50, 100, 50, 200, 'Sprite_894.png'),
('Zygarde', '50%', '718', 'Dragon', 'Sol', 'Rassemblement', 'Aura Inversée', NULL, NULL, 108, 100, 121, 81, 95, 95, 'Sprite_718_50.png'),
('Zygarde', 'Parfaite', '718', 'Dragon', 'Sol', 'Rassemblement', NULL, NULL, NULL, 216, 100, 121, 91, 95, 85, 'Sprite_718_Parfaite.png'),
('Zygarde', '10%', '718', 'Dragon', 'Sol', 'Rassemblement', 'Aura Inversée', NULL, NULL, 54, 100, 71, 61, 85, 115, 'Sprite_718_10.png'),
('Abo', 'Basique', '023', 'Poison', NULL, 'Intimidation', 'Mue', NULL, 'Tension', 35, 60, 44, 40, 54, 55, 'Sprite_023.png'),
('Arbok', 'Basique', '024', 'Poison', NULL, 'Intimidation', 'Mue', NULL, 'Tension', 60, 95, 69, 65, 79, 80, 'Sprite_024.png'),
('Pikachu', 'Basique', '025', 'Electrik', NULL, 'Statik', NULL, NULL, 'Paratonnerre', 35, 55, 40, 50, 50, 90, 'Sprite_025.png'),
('Raichu', 'Basique', '026', 'Electrik', NULL, 'Statik', NULL, NULL, 'Paratonnerre', 60, 90, 55, 90, 80, 110, 'Sprite_026.png'),
('Sabelette', 'Basique', '027', 'Sol', NULL, 'Voile Sable', NULL, NULL, 'Baigne Sable', 50, 75, 85, 20, 30, 40, 'Sprite_027.png'),
('Sablaireau', 'Basique', '028', 'Sol', NULL, 'Voile Sable', NULL, NULL, 'Baigne Sable', 75, 100, 110, 45, 55, 65, 'Sprite_028.png'),
('Nidoran (F)', 'Basique', '029', 'Poison', NULL, 'Point Poison', 'Rivalité', NULL, 'Agitation', 55, 47, 52, 40, 40, 41, 'Sprite_029.png'),
('Nidorina', 'Basique', '030', 'Poison', NULL, 'Point Poison', 'Rivalité', NULL, 'Agitation', 70, 62, 67, 55, 55, 56, 'Sprite_030.png'),
('Nidoqueen', 'Basique', '031', 'Poison', 'Sol', 'Point Poison', 'Rivalité', NULL, 'Sans Limite', 90, 92, 87, 75, 85, 76, 'Sprite_031.png'),
('Nidoran (M)', 'Basique', '032', 'Poison', NULL, 'Point Poison', 'Rivalité', NULL, 'Agitation', 46, 57, 40, 40, 40, 50, 'Sprite_032.png'),
('Nidorino', 'Basique', '033', 'Poison', NULL, 'Point Poison', 'Rivalité', NULL, 'Agitation', 61, 72, 57, 55, 55, 65, 'Sprite_033.png'),
('Nidoking', 'Basique', '034', 'Poison', 'Sol', 'Point Poison', 'Rivalité', NULL, 'Sans Limite', 81, 102, 77, 85, 75, 85, 'Sprite_034.png'),
('Mélofée', 'Basique', '035', 'Fée', NULL, 'Joli Sourire', 'Garde Magik', NULL, 'Garde-Ami', 70, 45, 48, 60, 65, 35, 'Sprite_035.png'),
('Mélodelfe', 'Basique', '036', 'Fée', NULL, 'Joli Sourire', 'Garde Magik', NULL, 'Inconscient', 95, 70, 73, 95, 90, 60, 'Sprite_036.png'),
('Goupix', 'Basique', '037', 'Feu', NULL, 'Torche', NULL, NULL, 'Sécheresse', 38, 41, 40, 50, 65, 65, 'Sprite_037.png'),
('Feunard', 'Basique', '038', 'Feu', NULL, 'Torche', NULL, NULL, 'Sécheresse', 73, 76, 75, 81, 100, 100, 'Sprite_038.png'),
('Rondoudou', 'Basique', '039', 'Normal', 'Fée', 'Joli Sourire', 'Battant', NULL, 'Garde-Ami', 115, 45, 20, 45, 25, 20, 'Sprite_039.png'),
('Grodoudou', 'Basique', '040', 'Normal', 'Fée', 'Joli Sourire', 'Battant', NULL, 'Fouille', 140, 70, 45, 85, 50, 45, 'Sprite_040.png'),
('Nosferapti', 'Basique', '041', 'Poison', 'Vol', 'Attention', NULL, NULL, 'Infiltration', 40, 45, 35, 30, 40, 55, 'Sprite_041.png'),
('Nosferalto', 'Basique', '042', 'Poison', 'Vol', 'Attention', NULL, NULL, 'Infiltration', 75, 80, 70, 65, 75, 90, 'Sprite_042.png'),
('Mystherbe', 'Basique', '043', 'Plante', 'Poison', 'Chlorophylle', NULL, NULL, 'Fuite', 45, 50, 5, 75, 65, 30, 'Sprite_043.png'),
('Ortide', 'Basique', '044', 'Plante', 'Poison', 'Chlorophylle', NULL, NULL, 'Puanteur', 60, 65, 70, 85, 75, 40, 'Sprite_044.png'),
('Rafflésia', 'Basique', '045', 'Plante', 'Poison', 'Chlorophylle', NULL, NULL, 'Pose Spore', 75, 80, 85, 110, 90, 50, 'Sprite_045.png'),
('Paras', 'Basique', '046', 'Insecte', 'Plante', 'Pose Spore', 'Peau Sèche', NULL, 'Moiteur', 35, 70, 55, 45, 55, 25, 'Sprite_046.png'),
('Parasect', 'Basique', '047', 'Insecte', 'Plante', 'Pose Spore', 'Peau Sèche', NULL, 'Moiteur', 60, 95, 80, 60, 80, 30, 'Sprite_047.png'),
('Mimitoss', 'Basique', '048', 'Insecte', 'Poison', 'Œil Composé', 'Lentiteintée', NULL, 'Fuite', 60, 55, 50, 40, 55, 45, 'Sprite_048.png'),
('Aéromite', 'Basique', '049', 'Insecte', 'Poison', 'Ecran Poudre', 'Lentiteintée', NULL, 'Peau Miracle', 70, 65, 60, 90, 75, 90, 'Sprite_049.png'),
('Taupiqueur', 'Basique', '050', 'Sol', NULL, 'Voile Sable', 'Piège Sable', NULL, 'Force Sable', 10, 55, 25, 35, 45, 95, 'Sprite_050.png'),
('Triopikeur', 'Basique', '051', 'Sol', NULL, 'Voile Sable', 'Piège Sable', NULL, 'Force Sable', 35, 100, 50, 50, 70, 120, 'Sprite_051.png'),
('Miaouss', 'Basique', '052', 'Normal', NULL, 'Ramassage', 'Technicien', NULL, 'Tension', 40, 45, 35, 40, 40, 90, 'Sprite_052.png'),
('Persian', 'Basique', '053', 'Normal', NULL, 'Echauffement', 'Technicien', NULL, 'Tension', 65, 70, 60, 65, 65, 115, 'Sprite_053.png'),
('Psykokwak', 'Basique', '054', 'Eau', NULL, 'Moiteur', 'Ciel Gris', NULL, 'Glissade', 50, 52, 48, 65, 50, 55, 'Sprite_054.png'),
('Akwakwak', 'Basique', '055', 'Eau', NULL, 'Moiteur', 'Ciel Gris', NULL, 'Glissade', 80, 82, 78, 95, 80, 85, 'Sprite_055.png'),
('Férosinge', 'Basique', '056', 'Combat', NULL, 'Esprit Vital', 'Colérique', NULL, 'Acharné', 40, 80, 35, 35, 45, 70, 'Sprite_056.png'),
('Colossinge', 'Basique', '057', 'Combat', NULL, 'Esprit Vital', 'Colérique', NULL, 'Acharné', 65, 105, 60, 60, 70, 95, 'Sprite_057.png'),
('Caninos', 'Basique', '058', 'Feu', NULL, 'Intimidation', 'Torche', NULL, 'Cœur Noble', 55, 70, 45, 70, 50, 60, 'Sprite_058.png'),
('Arcanin', 'Basique', '059', 'Feu', NULL, 'Intimidation', 'Torche', NULL, 'Cœur Noble', 90, 110, 80, 100, 80, 95, 'Sprite_059.png'),
('Ptitard', 'Basique', '060', 'Eau', NULL, 'Absorbe-Eau', 'Moiteur', NULL, 'Glissade', 40, 50, 40, 40, 40, 90, 'Sprite_060.png'),
('Tétarte', 'Basique', '061', 'Eau', NULL, 'Absorbe-Eau', 'Moiteur', NULL, 'Glissade', 65, 65, 65, 50, 50, 90, 'Sprite_061.png'),
('Tartard', 'Basique', '062', 'Eau', 'Combat', 'Absorbe-Eau', 'Moiteur', NULL, 'Glissade', 90, 95, 95, 70, 90, 70, 'Sprite_062.png'),
('Abra', 'Basique', '063', 'Psy', NULL, 'Synchro', 'Attention', NULL, 'Garde Magik', 25, 20, 15, 105, 55, 90, 'Sprite_063.png'),
('Kadabra', 'Basique', '064', 'Psy', NULL, 'Synchro', 'Attention', NULL, 'Garde Magik', 40, 35, 30, 120, 70, 105, 'Sprite_064.png'),
('Alakazam', 'Basique', '065', 'Psy', NULL, 'Synchro', 'Attention', NULL, 'Garde Magik', 55, 50, 45, 135, 95, 120, 'Sprite_065.png'),
('Machoc', 'Basique', '066', 'Combat', NULL, 'Cran', 'Annule Garde', NULL, 'Impassible', 70, 80, 50, 35, 35, 35, 'Sprite_066.png'),
('Machopeur', 'Basique', '067', 'Combat', NULL, 'Cran', 'Annule Garde', NULL, 'Impassible', 80, 100, 70, 50, 60, 45, 'Sprite_067.png'),
('Mackogneur', 'Basique', '068', 'Combat', NULL, 'Cran', 'Annule Garde', NULL, 'Impassible', 90, 130, 80, 65, 85, 35, 'Sprite_068.png'),
('Chetiflor', 'Basique', '069', 'Plante', 'Poison', 'Chlorophylle', NULL, NULL, 'Gloutonnerie', 50, 75, 35, 70, 30, 40, 'Sprite_069.png'),
('Boustiflor', 'Basique', '070', 'Plante', 'Poison', 'Chlorophylle', NULL, NULL, 'Gloutonnerie', 65, 90, 50, 85, 45, 55, 'Sprite_070.png'),
('Empiflor', 'Basique', '071', 'Plante', 'Poison', 'Chlorophylle', NULL, NULL, 'Gloutonnerie', 80, 105, 65, 100, 70, 70, 'Sprite_071.png'),
('Tentacool', 'Basique', '072', 'Eau', 'Poison', 'Corps Sain', 'Suintement', NULL, 'Cuvette', 40, 40, 35, 50, 100, 70, 'Sprite_072.png'),
('Tentacruel', 'Basique', '073', 'Eau', 'Poison', 'Corps Sain', 'Suintement', NULL, 'Cuvette', 80, 70, 65, 80, 120, 100, 'Sprite_073.png'),
('Racaillou', 'Basique', '074', 'Roche', 'Sol', 'Tête de Roc', 'Fermeté', NULL, 'Voile Sable', 40, 80, 100, 30, 30, 20, 'Sprite_074.png'),
('Gravalanch', 'Basique', '075', 'Roche', 'Sol', 'Tête de Roc', 'Fermeté', NULL, 'Voile Sable', 55, 95, 115, 45, 45, 35, 'Sprite_075.png'),
('Grolem', 'Basique', '076', 'Roche', 'Sol', 'Tête de Roc', 'Fermeté', NULL, 'Voile Sable', 80, 120, 130, 55, 65, 45, 'Sprite_076.png');

-- --------------------------------------------------------

--
-- Structure de la table `talent`
--

DROP TABLE IF EXISTS `talent`;
CREATE TABLE IF NOT EXISTS `talent` (
  `Nom` varchar(30) NOT NULL,
  `Description` varchar(10000) NOT NULL,
  PRIMARY KEY (`Nom`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Déchargement des données de la table `talent`
--

INSERT INTO `talent` (`Nom`, `Description`) VALUES
('Engrais', 'Un Pokémon doté de ce talent voit la puissance de ses capacités de type Plante augmentée de 50 % lorsque ses PV sont inférieurs à 1/3 de ses PV maximaux.');
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
