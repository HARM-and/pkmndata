<?php

include_once "../modele/BDManage.php";

class Talent
{
  private $BDD;
  function __construct()
  {
      $this->BDD = new Data;
      if (!isset($_SESSION))
      {
          session_start();
      }
  }

  function getTalent()
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from talent");
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

  function getTalentByName(string $Name)
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from talent where Nom=:Name");
          $req->bindValue(':Name', $Name, PDO::PARAM_STR);
          $req->execute();
          $resultat = $req->fetch(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }
}

?>