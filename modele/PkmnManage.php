<?php

include_once "../modele/BDManage.php";

class Pkmn
{
  private $BDD;
  function __construct()
  {
      $this->BDD = new Data;
      if (!isset($_SESSION))
      {
          session_start();
      }
  }

  function getPkmn()
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from pokemon group by N°");
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

    function countForm($nom)
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from pokemon where nom=:nom");
          $req->bindValue(':nom', $nom, PDO::PARAM_STR);
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return count($resultat);
    }

    function getForm($nom)
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from pokemon where nom=:nom");
          $req->bindValue(':nom', $nom, PDO::PARAM_STR);
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

    function getPkmnOrderByNom()
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from pokemon group by N° order by nom");
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

    function getPkmnOrderByType()
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from pokemon group by N° order by type_1");
          $req->execute();

          $resultat = $req->fetchAll(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

  function getPkmnByName(string $Name)
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from pokemon where Nom=:Name");
          $req->bindValue(':Name', $Name, PDO::PARAM_STR);
          $req->execute();
          $resultat = $req->fetch(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

    function getPkmnByForm(string $Name,string $Form)
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from pokemon where Nom=:Name and Forme=:Form");
          $req->bindValue(':Name', $Name, PDO::PARAM_STR);
          $req->bindValue(':Form', $Form, PDO::PARAM_STR);
          $req->execute();
          $resultat = $req->fetch(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

    function getPkmnByNum(string $No)
    {
      $resultat = array();
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select * from pokemon where N°=:No");
          $req->bindValue(':Name', $Name, PDO::PARAM_STR);
          $req->execute();
          $resultat = $req->fetch(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat;
    }

    function NumToName(string $No)
    {
      $resultat;
      try
        {
          $cnx = $this->BDD->connexionPDO();
          $req = $cnx->prepare("select Nom from pokemon where N°=:No");
          $req->bindValue(':No', $No, PDO::PARAM_STR);
          $req->execute();
          $resultat = $req->fetch(PDO::FETCH_ASSOC);
        }
        catch (PDOException $e)
        {
          print "Erreur !: " . $e->getMessage();
          die();
        }
      return $resultat["Nom"];
    }
}

function Color(int $stat)
{
  if ($stat<=50)
  {
    return "red";
  }
  else
  {
    if ($stat<=85)
    {
    return "orange";
    }
    else
    {
      if ($stat<=100)
      {
        return "yellow";
      }
      else
      {
        if ($stat<=175)
        {
          return "green";
        }
        else
        {
          return "blue";
        }
      }
    }
  }
}

function Secondary($value)
{
  if(isset($value))
  {
    return " / ".$value;
  }
}

function Invisible($value)
{
  if(isset($value))
  {
    return $value;
  }
  else
  {
    return "null.png\" height=\"1px\" width=\"1px";
  }
  
}

function Previous($value)
{
  $resultat = (string)((int)$value-1);

  while (strlen($resultat)<3)
  {
    $resultat = "0".$resultat;
  }

  if ($resultat=="000")
  {
    $resultat="001";
  }

  return $resultat;
}

function Nextious($value)
{
  $resultat = (string)((int)$value+1);

  while (strlen($resultat)<3)
  {
    $resultat = "0".$resultat;
  }

  if ($resultat=="899")
  {
    $resultat="898";
  }

  return $resultat;
}

//$test = new Pkmn;
//var_dump($test->countForm("Zygarde"));
//var_dump($test->getForm("Zygarde"));


?>